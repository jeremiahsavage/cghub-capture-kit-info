#!/bin/bash
#small script to compare the list of live analysis ids for <library_type> for <center> and then see how many are missing from that center's capture kit tsv for TCGA

center=$1
lib_type=$2
ck_tsv_path=$3
egrep -e	'^TCGA	' /cghub/tcga/www/html/ws/reports/SUMMARY_STATS/LATEST_MANIFEST.tsv | egrep -e "	${lib_type}	" | egrep -e '	Live	' | egrep -e "	${center}	" | cut -f 17 | sort -u > /tmp/${center}_${lib_type}_live_tcga.tsv.temp

cut -f 1 $ck_tsv_path | sort -u > /tmp/ck_tsv_uuids.txt.temp

fgrep -v -f /tmp/ck_tsv_uuids.txt.temp /tmp/${center}_${lib_type}_live_tcga.tsv.temp
