#!/usr/bin/perl
use strict;
#counts the non-overlapping coordinates in a bed file determining the total non-overlapping number of bases targeted
#assumes the contents are sorted by chromosome AND coordinate

my $target_file = shift;
main();

sub main
{
	my $coord_hash = preprocess_bed_file($target_file);
	count_bases_in_bed_file($target_file,$coord_hash);
}

sub preprocess_bed_file
{
	my $file = shift;
	#sort bed file here
	my %coords;
	open(IN,"<$file");
	while(my $line = <IN>)
	{
		chomp($line);
		my ($chr,$start,$end,$info)=split(/\t/,$line);
		push(@{$coords{$chr}->{$start}},$end);
	}
	close(IN);
	return \%coords;
}

sub count_bases_in_bed_file
{
	my ($old_file,$coord_hash) = @_;
	
	my $num_bases = 0;
	my $num_regions = 0;

	foreach my $chr (keys %$coord_hash)
	{
		foreach my $start (sort {$a <=> $b} keys %{$coord_hash->{$chr}})
		{
			my $ends = $coord_hash->{$chr}->{$start};
			my $end = $ends->[0];	
			if(scalar(@$ends) > 1)
			{
				my @a = sort {$a <=> $b} @$ends;
				$end = pop(@a);
				my $all_ends = join(";",@a);
				print STDERR "OVERLAP_DETECTED\t$old_file\t$start\t$end\t$all_ends\n";
				#die "OVERLAP_ERROR\t$old_file\t$chr\t$start\n";
			}
			$num_regions++;
			my $len = ($end-$start)+1;
			$num_bases += $len;
		}
	}
	close(IN);
	print "$old_file\t$num_regions\t$num_bases\n";
}
